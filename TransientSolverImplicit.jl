include("ComponentsForSolver.jl")

using .ComponentsForSolver
using LinearAlgebra

function TransientSolverImplicit()
    # Enter the file you want to run
    fileName="./Meshes/sphere2.msh"
    PhysicalGroups, PhysicalNames, nodeTags, nodeCoordinateOrdered, elementTags, entities,elementTags3D=ComponentsForSolver.ReadMeshScript(fileName)

    centerDict, nodeDict, connectDict, indexDict, entityDict,physNameGroupDict,nodeCoordDict=ComponentsForSolver.ConstructDictionaries( 
    nodeTags, nodeCoordinateOrdered, elementTags, entities,elementTags3D, fileName)

    normalDict, faceCenterDict, areaDict=ComponentsForSolver.ConstructNormals(centerDict, nodeDict, connectDict,nodeCoordDict)

    # Define material properties
    kVec=[
        1.0,
        1.0
    ]
    cpVec=[
        1.0,
        1.0
    ]
    densityVec=[
        1.0,
        1.0
    ]
    ΓDict=Dict{UInt64, Float64}()
    heatCapacityVec=zeros(length(collect(keys(connectDict))))

    for key1 in collect(keys(connectDict))
        VolType=entityDict[key1][2]
        ΓDict[key1]=kVec[VolType]
        heatCapacityVec[indexDict[key1]]=densityVec[VolType]*cpVec[VolType]
    end

    AM,volume,Δxvec,weightDict,gradMatrixDict=ComponentsForSolver.ConstructMatrix(ΓDict, centerDict, nodeDict, connectDict, 
    indexDict, nodeCoordDict, normalDict, faceCenterDict, areaDict)
    println("Δx_min är: ")
    println(minimum(vec(Δxvec)))
    println("volmin är : $(minimum(volume))")

    leftHandFactors=volume.*heatCapacityVec

    A=leftHandFactors.*AM

    t=0
    Δt=0.00005
    tmax=0.001
    N=ceil(Int,tmax/Δt)
    # Define initial condition:
    ϕ_0=ϕ_0=ones(size(AM,1))*273.15
    ϕ=[ϕ_0 zeros(size(AM,1),N)]
    for i=1:N
        error=1
        u=ϕ[:,i]
        u0=ϕ[:,i]
        its=0
        boundaryContributions,boundaryTemps=ComponentsForSolver.BoundaryContributions(ϕ[:,i], 
        connectDict, areaDict, physNameGroupDict, entityDict, t, centerDict, normalDict, indexDict,ΓDict)

        faceGradientsDict=ComponentsForSolver.LSGradient(ϕ[:,i], connectDict, indexDict, centerDict,faceCenterDict,gradMatrixDict,weightDict,boundaryTemps)

        crossDiffusionContributions=ComponentsForSolver.CrossDiffusionTerms(connectDict,indexDict,centerDict,
        normalDict, areaDict, faceGradientsDict,ΓDict)

        sourceTerms=boundaryContributions+crossDiffusionContributions
        F(x)=(AM*x+sourceTerms).*(leftHandFactors.^-1)
        uexp=ComponentsForSolver.EEulerOneStep(u0,F,Δt*0.5)
        u=uexp
        while error>1e-6

            boundaryContributions,boundaryTemps=ComponentsForSolver.BoundaryContributions(ϕ[:,i], 
            connectDict, areaDict, physNameGroupDict, entityDict, t, centerDict, normalDict, indexDict,ΓDict)
    
            faceGradientsDict=ComponentsForSolver.LSGradient(ϕ[:,i], connectDict, indexDict, centerDict,faceCenterDict,gradMatrixDict,weightDict,boundaryTemps)
    
            crossDiffusionContributions=ComponentsForSolver.CrossDiffusionTerms(connectDict,indexDict,centerDict,
            normalDict, areaDict, faceGradientsDict,ΓDict)

            sourceTerms=boundaryContributions+crossDiffusionContributions
            error=norm(uexp+0.5*Δt*(volume.^-1).*(AM*u+sourceTerms)-u,Inf)

            u=ComponentsForSolver.IEulerOSlinear(uexp,A,Diagonal(leftHandFactors.^-1)*sourceTerms,Δt*0.5)

            its+=1
            if its>100
                println("Max $(error)")
                break
            end
        end
        println("Number of iterations needed: $(its)")
        ϕ[:,i+1]=u
        if i%200==0
            println("Δt = $(Δt), Steps done: $(i) / $(N)")
        end
        if maximum(abs.(ϕ[:,i+1].-293.15))>30
            println("Unstable")
            break
        end
    end

    interiorKeys=collect(keys(connectDict))
    x=Array{Float64}(undef,size(interiorKeys,1))
    y=Array{Float64}(undef,size(interiorKeys,1))
    z=Array{Float64}(undef,size(interiorKeys,1))
    for key1 in interiorKeys
        x[indexDict[key1]]=centerDict[key1][1]
        y[indexDict[key1]]=centerDict[key1][2]
        z[indexDict[key1]]=centerDict[key1][3]
    end
    return ϕ, Δt, x,y,z,nodeDict,nodeCoordDict,indexDict
end
