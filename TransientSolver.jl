include("ComponentsForSolver.jl")

using .ComponentsForSolver
using LinearAlgebra

function TransientSolver()
    # Enter the file you want to run
    fileName="./Meshes/sphere2.msh"
    PhysicalGroups, PhysicalNames, nodeTags, nodeCoordinateOrdered, elementTags, entities,elementTags3D=ComponentsForSolver.ReadMeshScript(fileName)

    centerDict, nodeDict, connectDict, indexDict, entityDict,physNameGroupDict,nodeCoordDict=ComponentsForSolver.ConstructDictionaries( 
    nodeTags, nodeCoordinateOrdered, elementTags, entities,elementTags3D, fileName)

    normalDict, faceCenterDict, areaDict=ComponentsForSolver.ConstructNormals(centerDict, nodeDict, connectDict,nodeCoordDict)

    # Define material properties
    kVec=[
        1.0,
        1.0
    ]
    cpVec=[
        1.0,
        1.0
    ]
    densityVec=[
        1.0,
        1.0
    ]
    ΓDict=Dict{UInt64, Float64}()
    heatCapacityVec=zeros(length(collect(keys(connectDict))))

    for key1 in collect(keys(connectDict))
        VolType=entityDict[key1][2]
        ΓDict[key1]=kVec[VolType]
        heatCapacityVec[indexDict[key1]]=densityVec[VolType]*cpVec[VolType]
    end
    
    AM,volume,Δxvec,weightDict,gradMatrixDict=ComponentsForSolver.ConstructMatrix(ΓDict, centerDict, nodeDict, connectDict, 
    indexDict, nodeCoordDict, normalDict, faceCenterDict, areaDict)
    println("Δx_min är: ")
    println(minimum(vec(Δxvec)))
    println("volmin är : $(minimum(volume))")

    leftHandFactors=volume.*heatCapacityVec

    t=0
    Δt=minimum(cpVec.*densityVec)*minimum(Δxvec)^2/(6.1)
    tmax=0.2
    N=ceil(Int,tmax/Δt)
    # Define initial condition:
    ϕ_0=ϕ_0=ones(size(AM,1))*273.15
    ϕ=[ϕ_0 zeros(size(AM,1),N)]
    for i=1:N
        boundaryContributions,boundaryTemps=ComponentsForSolver.BoundaryContributions(ϕ[:,i], 
        connectDict, areaDict, physNameGroupDict, entityDict, t, centerDict, normalDict, indexDict,ΓDict)

        faceGradientsDict=ComponentsForSolver.LSGradient(ϕ[:,i], connectDict, indexDict, centerDict,faceCenterDict,gradMatrixDict,weightDict,boundaryTemps)

        crossDiffusionContributions=ComponentsForSolver.CrossDiffusionTerms(connectDict,indexDict,centerDict,
        normalDict, areaDict, faceGradientsDict,ΓDict)

        sourceTerms=boundaryContributions+crossDiffusionContributions

        # Define the function which describes ∂T/∂T
        F(x)=(AM*x+sourceTerms).*(leftHandFactors.^-1)
        ϕ[:,i+1]=ComponentsForSolver.EEulerOneStep(ϕ[:,i],F,Δt)
        if i%200==0
            println("Δt = $(Δt), Steps done: $(i) / $(N)")
        end
        if maximum(abs.(ϕ[:,i+1].-293.15))>300 # Vary this according to what you expect for the solution
            println("Unstable: $(i)")
            break
        end
    end

    interiorKeys=collect(keys(connectDict))
    x=Array{Float64}(undef,size(interiorKeys,1))
    y=Array{Float64}(undef,size(interiorKeys,1))
    z=Array{Float64}(undef,size(interiorKeys,1))
    for key1 in interiorKeys
        x[indexDict[key1]]=centerDict[key1][1]
        y[indexDict[key1]]=centerDict[key1][2]
        z[indexDict[key1]]=centerDict[key1][3]
    end
    return ϕ, Δt, x,y,z,nodeDict,nodeCoordDict,indexDict
end
