module ComponentsForSolver
include("./ComponentsForSolver/ReadMesh.jl")
include("./ComponentsForSolver/ConstructNormals.jl")
include("./ComponentsForSolver/ConstructMatrix.jl")
include("./ComponentsForSolver/BoundaryConditions.jl")
include("./ComponentsForSolver/CrossDiffusionContributions.jl")
include("./ComponentsForSolver/TimeStepMethods.jl")
include("./ComponentsForSolver/LSGradients.jl")
end
