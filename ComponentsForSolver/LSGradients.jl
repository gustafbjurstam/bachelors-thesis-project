function LSGradient(u, connectDict, indexDict, centerDict,faceCenterDict,gradMatrixDict,weightDict,boundaryTemps)
    #First compute gradients at centroids using Least Squares
    interiorKeys=collect(keys(connectDict))
    interiorKeysSet=Set(interiorKeys)
    nodalGradientDict=Dict{UInt64, Vector{Float64}}()
    for key1 in interiorKeys
        gM=gradMatrixDict[key1]
        weightVec=weightDict[key1]
        neighbours=connectDict[key1]
        uLocalVec=zeros(4)
        row=indexDict[key1]
        for i=1:4
            tempNeb=neighbours[i]
            if tempNeb ∈ interiorKeysSet
                uLocalVec[i]=u[indexDict[tempNeb]]-u[row]
            else
                AB=centerDict[tempNeb]-centerDict[key1]
                uLocalVec[i]=boundaryTemps[row][i]-u[row]
            end
        end
        uLocalVec=weightVec.*uLocalVec
        nodalGradientDict[key1]=gM * uLocalVec
    end
    # Interpolate gradients to faces
    faceGradientsDict=Dict{UInt64, Array{Vector{Float64}}}()
    for key1 in interiorKeys
        # Threads.@threads for key1 in interiorKeys
        neighbours=connectDict[key1]
        tempGrad=[[0.0, 0.0, 0.0],[0.0, 0.0, 0.0],[0.0, 0.0, 0.0],[0.0, 0.0, 0.0]]
        A=centerDict[key1]
        for i=1:4
            if neighbours[i]∈interiorKeysSet
                B=centerDict[neighbours[i]]
                face=faceCenterDict[(key1,neighbours[i])]
                AB=B-A
                dCe=norm(A-face)
                dEe=norm(B-face)
                gC=dCe/(dCe+dEe)
                gE=dEe/(dCe+dEe)
                tempGrad1=gC*nodalGradientDict[key1]+gE*nodalGradientDict[neighbours[i]]
                tempGrad[i]=((tempGrad1)+(u[indexDict[neighbours[i]]]-u[indexDict[key1]]-dot(tempGrad1,AB))*(norm(AB)^-2)*AB)
            end
        end
        faceGradientsDict[key1]=tempGrad
    end
    return faceGradientsDict
end