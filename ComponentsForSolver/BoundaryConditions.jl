using LinearAlgebra
function BoundaryContributions(u, connectDict, areaDict, physNameGroupDict, entityDict, t, centerDict, normalDict, indexDict, Γ)
    interiorKeys=Set(collect(keys(connectDict)))
    boundaryContributions=zeros(length(interiorKeys))
    boundaryTemps=Vector{Vector{Float64}}(undef,length(interiorKeys))
    # Information for convection boundary conditions
    h=10.0
    fluidTemperature=273.15+20
    convectionFunction(uLocal)=h*(fluidTemperature-uLocal)
    DconvectionFunction(uLocal)=-h
    #Information for radiation boundary conditions:
    radiationTemperature=273.15+18
    σ=5.670374419e-8
    emissivity=0.81
    radiationFunction(uLocal)=convectionFunction(uLocal)+σ*(radiationTemperature^4-uLocal^4)*emissivity
    DradiationFunction(uLocal)=DconvectionFunction(uLocal)-σ*emissivity*4*uLocal^3
    # Information for Neumann boundary conditions:
    totalEffect(t)= (0.1+0.8*(t>3600*8))*1.2e6#1.2e6 max for burner
    totalSurfaceArea=35.3284888032254#29.39#Beräknad vara 29.39 m^2
    efficiency=0.5
    distrubution(x)=(1.0 + 0.0*(x[3]-0.395))/ totalSurfaceArea
    neumannFunction(t,x)=efficiency*totalEffect(t)*distrubution(x)
    # Dirichlet:
    dirCond(t,x)=293.15
    # Compute the boundary contributions for each surface
    for key1 in interiorKeys
        row=indexDict[key1]
        A=centerDict[key1]
        neighbours=connectDict[key1]
        LocBdryTemps=zeros(4)
        for i=1:4
            if neighbours[i]∉interiorKeys
                area=areaDict[key1,neighbours[i]]
                B=centerDict[neighbours[i]]
                AB=B-A
                if occursin("Dirichlet", physNameGroupDict[entityDict[neighbours[i]]])
                    tempΓ=Γ[key1]
                    LocBdryTemps[i]=dirCond(t,B)
                    boundaryContributions[row]+=(dirCond(t,B)-u[row])*tempΓ*area/dot(AB,normalDict[key1][i])
                elseif occursin("Neumann", physNameGroupDict[entityDict[neighbours[i]]])
                    boundaryContributions[row]+=area*neumannFunction(t,B)
                    LocBdryTemps[i]=u[row]+neumannFunction(t,B)/(Γ[key1])*dot(AB,normalDict[key1][i])
                elseif occursin("Radiation", physNameGroupDict[entityDict[neighbours[i]]])
                    radFnc2(uLoc)=radiationFunction(uLoc)/(Γ[key1])
                    DradFnc2(uLoc)=DradiationFunction(uLoc)/(Γ[key1])
                    uLoc=NewtonsMethod(radFnc2,DradFnc2,u[row],dot(AB,normalDict[key1][i]))
                    LocBdryTemps[i]=uLoc
                    boundaryContributions[row]+=area*radiationFunction(uLoc)
                elseif occursin("Convection", physNameGroupDict[entityDict[neighbours[i]]])
                    conFnc2(uLoc)=convectionFunction(uLoc)/(Γ[key1])
                    DconFnc2(uLoc)=DconvectionFunction(uLoc)/(Γ[key1])
                    uLoc=NewtonsMethod(conFnc2,DconFnc2,u[row],dot(AB,normalDict[key1][i]))
                    LocBdryTemps[i]=uLoc
                    boundaryContributions[row]+=area*convectionFunction(uLoc)
                else # Isolated
                    LocBdryTemps[i]=u[row]
                end
            end
        end
        boundaryTemps[row]=LocBdryTemps
    end
    return boundaryContributions,boundaryTemps
end

function NewtonsMethod(f,df,x0,δ)
    g(x)=x-δ*f(x)-x0
    dg(x)=1-δ*df(x)
    x=x0-g(x0)/dg(x0)
    error=abs(g(x))
    while error>δ*1e-6
        x=x-g(x)/dg(x)
        error=abs(g(x))
    end
    return x
end