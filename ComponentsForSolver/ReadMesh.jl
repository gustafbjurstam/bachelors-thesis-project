import Gmsh: gmsh

function ReadMeshScript(fileName="sphere.msh")
    gmsh.initialize()
    gmsh.open(fileName)

    PhysicalGroups=gmsh.model.getPhysicalGroups();

    n=size(PhysicalGroups,1)
    PhysicalNames=Array{String}(undef,n)
    for i in 1:n
        PhysicalNames[i]=gmsh.model.getPhysicalName(PhysicalGroups[i][1],PhysicalGroups[i][2])
    end
    nodeTags,_,_=gmsh.model.mesh.getNodes()
    nodeCoordinateOrdered=Array{Any}(undef,size(nodeTags,1))
    for i in eachindex(nodeTags)
        nodeCoordinateOrdered[i],_,_,_=gmsh.model.mesh.getNode(nodeTags[i])
    end
    entities=vcat(gmsh.model.getEntities(2), gmsh.model.getEntities(3))
    elementTags=Array{Vector}(undef,0)
    elementTags3D=Array{Vector}(undef,0)
    i=1
    println(entities)
    entities2=Vector{Tuple{Int32,Int32}}(undef,0)
    for dimTag in entities
        _,tempTags,=gmsh.model.mesh.getElements(dimTag[1],dimTag[2])
        # println("Success: $(i)")
        if 0!=length(tempTags)
            append!(elementTags,[tempTags[1]])
            append!(entities2,[dimTag])
            i+=1
            if dimTag[1]==3
                append!(elementTags3D,[tempTags[1]])
            end
        end
    end
    gmsh.finalize()
    return PhysicalGroups, PhysicalNames, nodeTags, nodeCoordinateOrdered, elementTags, entities2,elementTags3D
end

function ConstructDictionaries(nodeTags, nodeCoordinateOrdered, elementTags, entities,elementTags3D, fileName="sphere.msh", )
    # PhysicalGroups, PhysicalNames, nodeTags, nodeCoordinateOrdered, elementTags, entities=ReadMeshScript(fileName)
    entityDict=Dict{UInt64, Tuple{Int32,Int32}}()
    # println(entities)
    for (index,tags) in enumerate(elementTags)
        # println((index,tags))
        tempDict=Dict(tags.=>[entities[index]])
        entityDict=merge(entityDict,tempDict)
    end
    # println(entityDict)
    gmsh.initialize()
    gmsh.open(fileName)
    centerDict=Dict{UInt64, Vector{Float64}}()
    nodeDict=Dict{UInt64, Array{UInt64}}()
    i=1
    for element in vcat(elementTags...)
        _,tempNodes,=gmsh.model.mesh.getElement(element)
        avgnode=zeros(3)
        n=size(tempNodes,1)
        for node in tempNodes
            nodeCoord,=gmsh.model.mesh.getNode(node)
            avgnode=avgnode+nodeCoord*(1/n)
        end
        centerDict[element]=avgnode
        nodeDict[element]=tempNodes
        i+=1
    end
    #time to connect interior elements to their neighbours
    connectDict=Dict{UInt64, Vector{UInt64}}()
    # println(elementTags3D)
    for element in vcat(elementTags3D...)# Supposing the interior is just one entity this is fine, not that this may not be the case, depending on implementation of variable Γ
        tempNodes=Set(nodeDict[element])
        connectors=Array{UInt64}(undef,4) # 4 is for 3d-Case
        i=1
        # println("$(element)")
        for element2 in vcat(elementTags...)
            if length(intersect(tempNodes,nodeDict[element2]))==3
                connectors[i]=element2
                i+=1
                # print("1")
            end
        end
        connectDict[element]=connectors
    end
    indexDict=Dict(vcat(elementTags3D...).=> 1:length(vcat(elementTags3D...)))
    # Should create a dictionary that maps entities to a physical name i guess
    physNameGroupDict=Dict{Tuple{Int32,Int32},String}()
    for ent in entities
        tempTags=gmsh.model.getPhysicalGroupsForEntity(ent[1], ent[2])
        # println(tempTags[1])
        physNameGroupDict[ent]=gmsh.model.getPhysicalName(ent[1], tempTags[1])
    end
    gmsh.finalize()
    nodeCoordDict=Dict(nodeTags.=>nodeCoordinateOrdered)
    return centerDict, nodeDict, connectDict, indexDict, entityDict,physNameGroupDict,nodeCoordDict
end