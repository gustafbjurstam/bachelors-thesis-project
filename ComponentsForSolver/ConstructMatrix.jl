using LinearAlgebra
using SparseArrays

function ConstructMatrix(Γ,centerDict, nodeDict, connectDict, 
    indexDict,nodeCoordDict,normalDict, faceCenterDict,areaDict)
    # Function starts below
    interiorKeys=collect(keys(connectDict))
    AM=spzeros(Float64,size(interiorKeys,1),size(interiorKeys,1))
    volume=zeros(Float64,size(interiorKeys,1))
    Δxvec=zeros(Float64,size(interiorKeys,1),4)
    for key1 in interiorKeys
        row=indexDict[key1]
        A=centerDict[key1]
        neighbours=connectDict[key1]
        for i=1:4
            B=centerDict[neighbours[i]]
            Δxvec[indexDict[key1],i]=norm(B-A)
            if neighbours[i]∈interiorKeys
                AB=B-A
                # Do harmonic mean of the two volumes for Γ
                tempΓ=2*Γ[key1]*Γ[neighbours[i]]/(Γ[key1]+Γ[neighbours[i]])
                area=areaDict[key1,neighbours[i]]
                AM[row,row]=AM[row,row]-tempΓ*area/dot(AB,normalDict[key1][i])
                AM[row,indexDict[neighbours[i]]]=tempΓ*area/dot(AB,normalDict[key1][i])
            else
                # Flux-defining boundary condtion, treated purely as source terms.
            end
        end
        volMatrix=[nodeCoordDict[nodeDict[key1][1]][1:3] nodeCoordDict[nodeDict[key1][2]][1:3] nodeCoordDict[nodeDict[key1][3]][1:3] nodeCoordDict[nodeDict[key1][4]][1:3]]
        volMatrix=[volMatrix; [1 1 1 1]]
        volume[row]=abs(det(volMatrix))/6 
    end
    # return AM, volume,Δxvec

    #Create matrices for LS-grad
    gradMatrixDict=Dict{UInt64, Matrix{Float64}}()
    weightDict=Dict{UInt64, Vector{Float64}}()
    for key1 in interiorKeys
        A=centerDict[key1]
        gradMatrix=zeros(4,3)
        neighbours=connectDict[key1]
        weightVec=zeros(4)
        for i=1:4
            tempNeb=neighbours[i]
            B=centerDict[tempNeb]
            AB=B-A
            weight=norm(AB)^-1
            gradMatrix[i,:]=weight*AB
            weightVec[i]=weight
        end
        gradMatrixDict[key1]=inv(transpose(gradMatrix)*gradMatrix)*transpose(gradMatrix)
        weightDict[key1]=weightVec
    end
    return AM, volume,Δxvec, weightDict,gradMatrixDict
end