function ConstructNormals(centerDict, nodeDict, connectDict,nodeCoordDict)
    interiorKeys=collect(keys(connectDict))
    normalDict=Dict{UInt64, Vector{Vector{Float64}}}()
    faceCenterDict=Dict{Tuple{UInt64, UInt64}, Vector{Float64}}()
    areaDict=Dict{Tuple{UInt64, UInt64}, Float64}()
    for key1 in interiorKeys
        neighbours=connectDict[key1]
        normals=Array{Vector{Float64}}(undef,4)
        for i=1:4
            # println((key1,neighbours[i]))
            nodes=intersect(nodeDict[key1],nodeDict[neighbours[i]])
            normalWrongSize=cross((nodeCoordDict[nodes[1]]-nodeCoordDict[nodes[2]]),(nodeCoordDict[nodes[1]]-nodeCoordDict[nodes[3]]))
            normals[i]=sign(dot(normalWrongSize,centerDict[neighbours[i]]-centerDict[key1]))*normalWrongSize/norm(normalWrongSize)
            faceCenterDict[(key1,neighbours[i])]=(nodeCoordDict[nodes[1]]+nodeCoordDict[nodes[2]]+nodeCoordDict[nodes[3]])*1/3
            areaDict[(key1,neighbours[i])]=norm(normalWrongSize)/2
        end
        normalDict[key1]=normals
    end
    return normalDict, faceCenterDict, areaDict
end