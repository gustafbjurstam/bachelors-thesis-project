using LinearAlgebra

function CrossDiffusionTerms(connectDict,indexDict,centerDict,
    normalDict, areaDict, faceGradientsDict,Γ)
    interiorKeys=Set(collect(keys(connectDict)))
    crossDiffusionContributions=zeros(length(interiorKeys))
    for key1 in interiorKeys
        row=indexDict[key1]
        A=centerDict[key1]
        neighbours=connectDict[key1]
        for i=1:4
            if neighbours[i]∈interiorKeys
                B=centerDict[neighbours[i]]
                AB=A-B
                normal=normalDict[key1][i]
                correctionVector=normal-(AB)/(dot((AB),normal))
                CDatface=dot(correctionVector,faceGradientsDict[key1][i])*areaDict[(key1,neighbours[i])]
                tempΓ=2*Γ[key1]*Γ[neighbours[i]]/(Γ[key1]+Γ[neighbours[i]])
                crossDiffusionContributions[row] += CDatface*tempΓ
            end
        end
    end
    return crossDiffusionContributions
end