using LinearAlgebra

function EEulerOneStep(u0,f,dt)
    u=u0+dt*f(u0)
    return u
end

function IEulerOneStep(u0,f,dt)
    g(x)=u0-x+dt*f(x)
    u=NLsolve.nlsolve(g,u0)
    return u
end

function IEulerOSlinear(u0,A,s,dt)
    u=(I-dt*A)\(u0+dt*s)
    dudt=A*u+s
    return u
end

function AB2(u2,f,f1,dt)
    f2=f(u2)
    u3=u2+0.5*dt*(3*f2-f1)
    return u3, f2
end
# It is not clear whether AB2 could possibly be stable, it would however be nice as it would be second order
#  (for real) in time, while also being as cheap to compute as EEuler, only requíring very little extra memory
# For the first step it is enough to take a step with EEuler as EEuler has second order local accuracy.