# Bachelors thesis project KTH
## Construction and evaluation of a numerical model for heat transfer in a ladle during pre-heating -- A Finite Volume Approach to the Diffusion Equation using Julia

This was the code I developed for my bachelors thesis in Materials Science and Engineering at KTH in the spring 2023.
The full report can be found [HERE](https://urn.kb.se/resolve?urn=urn:nbn:se:kth:diva-331267)

IEEE citation:\
G. Bjurstam, ‘Construction and Evaluation of a Numerical Model for Heat Transfer in a Ladle During Pre-heating : A Finite Volume Approach to the Diffusion Equation using Julia’, Dissertation, 2023.

## What  does the code do?

The code solves the diffusion equation in three dimensions using a finite volume methodology by reading tetrahedral meshes generated using [Gmsh](http://gmsh.info/).
The solver can handle the following kinds of boundary conditions:

- Dirichlet,
- Neumann,
- Robin,
- The kind of non-linear boundary condition which arises from radiation at the boundary of the domain.

Other kinds of non-linear boundary conditions can easily be implemented as needed.

## How are parameters handled?

The type of boundary condtion is defined by the name of the physical surface in the ``.msh``-file. The parameters for the boundary condtion, e.g. the temperature defined by a Dirichlet-condtion, is defined in ``BoundaryConditions.jl``. With the current implementation each kind of bounadry condition is only able to handle one definition, this should however be quite easy to remedy by any programmer if it is needed.

For each physical volume defined by the ``.msh``-file you can define diffusivities, heat capacities, and densities, by changing the values of `kVec,densityVec,cpVec` in the main script you are running.

## Project status and todo-list

While the state of this project at time of publication of the BSc. thesis will "always" be preserved here I do plan to make some improvements to the project, most of these are discussed in section 5.4 of the thesis.
The development might be very slow as i will have many other things to do in my studies.

## Author contact

You can contact me, Gustaf Bjurstam, by emailing <bjurstam@kth.se>.

## License
This project is available under the GNU GPLv3 license. See the COPYING file for more info.
